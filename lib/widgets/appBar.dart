import 'package:flutter/material.dart';
import 'package:pet_adpt/user/profile.dart';
import 'package:pet_adpt/user/settings.dart';
import 'package:pet_adpt/user/upload.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      title: Row(
        children: [
          Expanded(
            child: Image.asset(
              'assets/logo.png',
              height: 90,
            ),
          ),
          const Expanded(
            flex: 2,
            child: Text(
              'P.A.',
              style: TextStyle(
                  fontFamily: 'Neuton', fontSize: 36, color: Colors.black),
            ),
          ),
        ],
      ),

      //BUTTONS
      actions: [
        IconButton(
            icon: Icon(Icons.upload, color: Theme.of(context).primaryColor),
            onPressed: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: ((context) => upload())))),
        IconButton(
          icon: Icon(Icons.settings, color: Theme.of(context).primaryColor),
          onPressed: () => Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => signOut())),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(56.0);
}
