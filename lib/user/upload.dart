import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:pet_adpt/start/home_screen.dart';

class upload extends StatefulWidget {
  @override
  _uploadState createState() => _uploadState();
}

class _uploadState extends State<upload> {
  File? _image;
  final picker = ImagePicker();
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController breedController = TextEditingController();
  TextEditingController contact_infoController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future uploadImage() async {
    if (_image == null) {
      print('No image selected.');
      return;
    }

    final uri = Uri.parse(
        "http://192.168.43.212/pet_adaption_web/upload.php"); // Replace with your server URL

    final request = http.MultipartRequest('POST', uri);
    request.fields['name'] = nameController.text;
    request.fields['age'] = ageController.text;
    request.fields['breed'] = breedController.text;
    request.fields['contact_info'] = contact_infoController.text;
    request.fields['description'] = descriptionController.text;
    request.files.add(http.MultipartFile(
      'image',
      _image!.readAsBytes().asStream(),
      _image!.lengthSync(),
      filename: _image!.path,
    ));

    final response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Image and data uploaded successfully'),
        ),
      );
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      print('Image upload failed');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Image upload failed'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: const BoxDecoration(
              gradient: LinearGradient(colors: [
                Color(0xff000000),
                Color(0xff808080),
              ]),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                    bottomLeft: Radius.circular(40),
                    bottomRight: Radius.circular(40)),
                color: Colors.white,
              ),
              height: double.infinity,
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(left: 18.0, right: 18, top: 50),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 200,
                        width: 200,
                        child: _image == null
                            ? Text(
                                'No image selected.',
                                textAlign: TextAlign.center,
                              )
                            : Image.file(_image!),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        height: 30,
                        width: 100,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          gradient: const LinearGradient(colors: [
                            Color(0xff000000),
                            Color(0xff808080),
                          ]),
                        ),
                        child: TextButton(
                          child: const Text(
                            'select image',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            getImage();
                          },
                        ),
                      ),
                      TextField(
                        controller: nameController,
                        decoration: InputDecoration(
                            label: Text(
                          'Name',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff000000),
                          ),
                        )),
                      ),
                      TextField(
                        controller: ageController,
                        decoration: InputDecoration(
                            label: Text(
                          'Age',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff000000),
                          ),
                        )),
                      ),
                      TextField(
                        controller: breedController,
                        decoration: InputDecoration(
                            label: Text(
                          'Breed',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff000000),
                          ),
                        )),
                      ),
                      TextField(
                        controller: contact_infoController,
                        decoration: InputDecoration(
                            label: Text(
                          'Contact Info',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff000000),
                          ),
                        )),
                      ),
                      TextField(
                        controller: descriptionController,
                        decoration: InputDecoration(
                            label: Text(
                          'Description',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff000000),
                          ),
                        )),
                      ),
                      SizedBox(
                        height: 70,
                      ),
                      Container(
                        height: 48,
                        width: 200,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(colors: [
                            Color(0xff000000),
                            Color(0xff808080),
                          ]),
                        ),
                        child: TextButton(
                          child: const Text(
                            'upload image',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.white),
                          ),
                          onPressed: () {
                            uploadImage();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 35,
            left: 10,
            child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                }),
          ),
        ],
      ),
    );
  }
}
