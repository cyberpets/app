import 'package:flutter/material.dart';

class FormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String? name;
    String? contactNumber;
    String? description;

    return Scaffold(
      appBar: AppBar(
        title: Text('Form Page'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: 'Name'),
                onChanged: (value) {
                  name = value;
                },
              ),
              SizedBox(height: 16.0),
              TextFormField(
                decoration: InputDecoration(labelText: 'Contact Number'),
                onChanged: (value) {
                  contactNumber = value;
                },
              ),
              SizedBox(height: 16.0),
              TextFormField(
                decoration: InputDecoration(labelText: 'Description'),
                maxLines: 4,
                onChanged: (value) {
                  description = value;
                },
              ),
              SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () {
                  if (name != null &&
                      contactNumber != null &&
                      description != null) {
                    // Use the 'name', 'contactNumber', and 'description' variables
                    // to send the form data to the email or perform any other action.

                    // After sending the email or completing the action,
                    // you can navigate back to the Details page.
                    Navigator.pop(context);
                  } else {
                    // Handle the case when not all fields are filled.
                    // You can show an error message or take appropriate action.
                  }
                },
                child: Text('Send'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
