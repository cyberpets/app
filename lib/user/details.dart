import 'package:flutter/material.dart';
import 'package:pet_adpt/models/pet.dart';
import 'package:pet_adpt/user/form.dart';

class Details extends StatelessWidget {
  final Pet pet;

  Details({required this.pet});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.only(
            left: 16.0, right: 16.0, top: 40.0), // Adjust the top padding value
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                Text(
                  'Pet Details',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 8.0),
            Text(
              'Name: ${pet.name}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              'Breed: ${pet.breed}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              'Age: ${pet.age}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              'Contact: ${pet.contact_info}',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 16.0),
            Text(
              'Description:',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8.0),
            Text(
              pet.description,
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              height: 500, // Set your desired height
              width: 400, // Set your desired width
              alignment: Alignment.center, // Align the image to the center
              child: Padding(
                padding: EdgeInsets.only(top: 20.0), // Add a margin at the top
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                      20.0), // Set your desired corner radius
                  child: Image.network(
                    pet.image_path,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
