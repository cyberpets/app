import 'package:flutter/material.dart';
import 'package:pet_adpt/start/log_in.dart';

class signOut extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: const BoxDecoration(
              gradient: LinearGradient(colors: [
                Color(0xff000000),
                Color(0xff808080),
              ]),
            ),
          ),
          Positioned(
            top: 40.0,
            left: 10.0,
            child: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white, // Customize the icon color
              ),
              onPressed: () {
                Navigator.of(context)
                    .pop(); // Add functionality for the back button
              },
            ),
          ),
          Positioned(
            top: 50.0,
            left: 60.0,
            child: Text(
              'Settings',
              style: TextStyle(
                fontSize: 30,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 100.0),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40)),
                color: Colors.white,
              ),
              height: double.infinity,
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.only(left: 18.0, right: 18, top: 50),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft, // Align text to the left
                      child: Text(
                        'Want to Log out?',
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        // Perform logout action here, such as clearing user credentials.
                        // Then, navigate to the login screen and remove the settings screen from the stack.
                        Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                            builder: (context) =>
                                LoginScreen(), // Replace with your login screen widget
                          ),
                          (route) =>
                              false, // Remove all existing routes from the stack.
                        );
                      },
                      child: Container(
                        height: 50,
                        width: 500,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(colors: [
                            Color(0xff000000),
                            Color(0xff808080),
                          ]),
                        ),
                        child: Center(
                          child: Text(
                            'Log out',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
