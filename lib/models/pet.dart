class Pet {
  final String name;
  final String age;
  final String breed;
  final String contact_info;
  final String description;
  final String image_path;

  Pet({
    required this.name,
    required this.age,
    required this.breed,
    required this.contact_info,
    required this.description,
    required this.image_path,
  });

  factory Pet.fromJson(Map<String, dynamic> json) {
    final name = json['name'] as String? ?? '';
    final age = json['age'] as String? ?? '';
    final breed = json['breed'] as String? ?? '';
    final contact_info = json['contact_info'] as String? ?? '';
    final description = json['description'] as String? ?? '';
    final image_path = json['image_path'] as String? ?? '';

    return Pet(
      name: name,
      age: age,
      breed: breed,
      contact_info: contact_info,
      description: description,
      image_path: image_path,
    );
  }
}
