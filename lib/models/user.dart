class User {
  final String first_name;
  final String last_name;
  final String username;
  final String email;

  User({
    required this.first_name,
    required this.last_name,
    required this.username,
    required this.email,
  });
}
