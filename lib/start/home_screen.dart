import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pet_adpt/models/pet.dart';
import 'package:pet_adpt/user/details.dart';
import 'package:pet_adpt/user/profile.dart';
import 'package:pet_adpt/widgets/appBar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Pet> pets = [];
  int currentPetIndex = 0;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void _swipeLeft() {
    setState(() {
      currentPetIndex = (currentPetIndex - 1) % pets.length;
    });
  }

  void _swipeRight() {
    setState(() {
      currentPetIndex = (currentPetIndex + 1) % pets.length;
    });
  }

  Future<void> fetchData() async {
    final uri =
        Uri.parse('http://192.168.43.212/pet_adaption_web/pet_details.php');

    try {
      final response = await http.get(uri);

      if (response.statusCode == 200) {
        final List<dynamic> dataList = json.decode(response.body);

        setState(() {
          pets = dataList.map((petData) => Pet.fromJson(petData)).toList();
        });
      }
    } catch (e) {
      Fluttertoast.showToast(msg: "error fetching data: $e");
      print('Error fetching pets: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Padding(
        padding: EdgeInsets.only(
            top: 40.0), // Adjust the top padding value as needed
        child: Column(
          children: [
            Expanded(
              child: Stack(
                children: pets.asMap().entries.map((entry) {
                  final index = entry.key;
                  final pet = entry.value;

                  if (index < currentPetIndex) {
                    return Container(); // Already swiped.
                  } else if (index == currentPetIndex) {
                    return Draggable(
                      onDragEnd: (details) {
                        if (details.offset.dx > 50) {
                          _swipeRight();
                        } else if (details.offset.dx < -50) {
                          _swipeLeft();
                        }
                      },
                      feedback: SwipeCard(
                          pet: pet,
                          onSwipeRight: _swipeRight,
                          onSwipeLeft: _swipeLeft),
                      child: SwipeCard(
                          pet: pet,
                          onSwipeRight: _swipeRight,
                          onSwipeLeft: _swipeLeft),
                      childWhenDragging: Container(),
                    );
                  } else {
                    return Container(); // Upcoming cards.
                  }
                }).toList(),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.black, // Add your desired border color
                  width: 2.0, // Adjust the border width as needed
                ),
              ),
              child: IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  // Implement search action
                },
              ),
            ),
            SizedBox(width: 20), // Add spacing between the icons
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.black, // Add your desired border color
                  width: 2.0, // Adjust the border width as needed
                ),
              ),
              child: IconButton(
                icon: Icon(Icons.person),
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfilePage(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SwipeCard extends StatelessWidget {
  final Pet pet;
  final Function onSwipeRight;
  final Function onSwipeLeft;

  SwipeCard(
      {required this.pet,
      required this.onSwipeRight,
      required this.onSwipeLeft});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) {
        if (details.delta.dx > 50) {
          onSwipeRight();
        } else if (details.delta.dx < -50) {
          onSwipeLeft();
        }
      },
      child: Card(
        elevation: 6.0,
        margin: EdgeInsets.all(0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        child: InkWell(
          onTap: () {
            // Navigate to the pet's details page.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => Details(pet: pet),
              ),
            );
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Name: ${pet.name}',
                  style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  'Breed: ${pet.breed}',
                  style: TextStyle(fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
                Container(
                  height: 480,
                  width: 500,
                  child: Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: Image.network(
                        pet.image_path,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
